public class TrainCar {

    WildCat cat;
    TrainCar next;

    public static final double EMPTY_WEIGHT = 20; // In kilograms

    //constructor if theres no car before this one
    public TrainCar(WildCat cat) {
        this.cat = cat;
    }

    //constructor if there's already another car before this one
    public TrainCar(WildCat cat, TrainCar next) {
        this.cat = cat;
        this.next = next;
    }

    //calculate all the cat and car weight
    public double computeTotalWeight() {
        if(next==null){
            return EMPTY_WEIGHT + cat.weight;
        }
        else{
            return EMPTY_WEIGHT + cat.weight + next.computeTotalWeight();
        }
    }

    //calculate the BMI of all cat
    public double computeTotalMassIndex() {
        if(next==null){
            return cat.computeMassIndex();
        }
        else{
            return cat.computeMassIndex() + next.computeTotalMassIndex();
        }
    }

    //print that cat name inside the car
    public void printCar() {
        if(next==null){
            System.out.print("(" + cat.name + ")");
        }
        else{
            System.out.print("(" + cat.name + ")--");
            next.printCar();
        }
    }
}
