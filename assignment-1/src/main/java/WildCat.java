public class WildCat {
    String name;
    double weight; // In kilograms
    double length; // In centimeters

    //constructor
    public WildCat(String name, double weight, double length) {
        this.name = name;
        this.weight = weight;
        this.length = length;
    }

    //calculate the BMI of the cat
    public double computeMassIndex() {
        double BMI = this.weight * 10000 / (this.length * this.length);
        return BMI;
    }
}
