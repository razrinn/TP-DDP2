import java.util.Scanner;

public class A1Station {

    private static final double THRESHOLD = 250; // in kilograms

    public static void main(String[] args) {
    	Scanner input = new Scanner(System.in);
        TrainCar kereta = null;

        //assign variable that we need
        int kucingKereta = 0;
        int jumlah = Integer.parseInt(input.nextLine());
        int sisaKucing = jumlah;

        //start looping
        for (int i = 0; i < jumlah; i++) {
        	String[] arrKucing = input.nextLine().split(",");
        	String namaKucing = arrKucing[0];
        	double beratKucing = Double.parseDouble(arrKucing[1]);
        	double panjangKucing = Double.parseDouble(arrKucing[2]);

        	//create new WildCat object
        	WildCat kucing = new WildCat(namaKucing, beratKucing, panjangKucing);
        	kucingKereta++;
        	sisaKucing--;

        	//selection if theres a car or not before
        	if(kereta==null){
        		kereta = new TrainCar(kucing);
        	}
        	else{
        		kereta = new TrainCar(kucing, kereta);
        	}

        	//find the average of the cat BMI
        	double rataSemua = kereta.computeTotalMassIndex() / kucingKereta;
        	String kategori = "";

        	//selection if the total weight is more than 250 or not , and when there is not cat left to be proccessed
        	if(kereta.computeTotalWeight() > 250 || sisaKucing == 0){
        		System.out.println("The train departs to Javari Park.");
        		System.out.print("[LOCO]<--");
        		kereta.printCar();
        		System.out.println("\nAverage mass index of all cats: " + String.format("%.2f", rataSemua));

        		//selection about average cat in the train's BMI category
        		if(rataSemua >= 30){
        			kategori = "*obese*";
        		}
        		else if(25 <= rataSemua  && rataSemua < 30){
        			kategori = "*overweight*";
        		}
        		else if(18.5 <= rataSemua && rataSemua < 25){
        			kategori = "*normal*";
        		}
        		else{
        			kategori = "*underweight*";
        		}
        		System.out.println("In average, the cats in the train are " + kategori);

        		//reset the train and the cat on the train
        		kereta = null;
        		kucingKereta = 0;


        	}
        }


    }
}
