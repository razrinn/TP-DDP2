package javari.reader;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;

public class CategoryReader extends CsvReader {
    private static int checkInt = 0;
    private static ArrayList<String> sections = new ArrayList<>();

    public CategoryReader(Path path) throws IOException {
        super(path);
    }

    public static ArrayList<String> getSections() {
        return sections;
    }

    @Override
    public long countValidRecords() {
        boolean isValid = false;
        int check = 0;
        ArrayList<String> validRec = new ArrayList<>();

        for (String line : lines) {
            String[] input = line.split(COMMA);
            if (getMapAnimalKingdom().containsKey(input[1])) {
                isValid = getMapAnimalKingdom().get(input[1]).contains(input[0]);
            }
            isValid &= ((check != 0) || getMapAnimal().containsKey(input[2]));
            if (isValid) {
                if (!validRec.contains(input[2])) {
                    validRec.add(input[2]);
                }
            }
        }
        sections = (checkInt++ == 0) ? new ArrayList<>(validRec) : sections;
        return validRec.size();
    }

    @Override
    public long countInvalidRecords() {
        long counter = 0;

        for (String line : lines) {
            String[] input = line.split(COMMA);
            boolean isValid = true;
            if (getMapAnimalKingdom().containsKey(input[1])) {
                isValid = getMapAnimalKingdom().get(input[1]).contains(input[0]);
            }
            isValid &= ((checkInt != 0) || getMapAnimal().containsKey(input[2]));
            if (!isValid) {
                counter++;
            }
        }
        return counter;
    }
}
