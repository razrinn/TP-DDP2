package javari.reader;

import javari.animal.*;
import javari.park.Attraction;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;

public class RecordReader extends CsvReader {
    private static long valid;
    private static long total;

    public RecordReader(Path path) throws IOException {
        super(path);
    }

    @Override
    public long countValidRecords() {
        valid = 0;
        total = 0;

        for (String line : lines) {
            String[] input = line.split(COMMA);
            Animal temp;
            total++;

            if (mapAnimalKingdom.get("mammals").contains(input[1])) {
                temp = new Mammal(Integer.parseInt(input[0]), input[1], input[2], Gender.parseGender(input[3]), Double.parseDouble(input[4]), Double.parseDouble(input[5]), input[6], Condition.parseCondition(input[7]));
            } else if (mapAnimalKingdom.get("aves").contains(input[1])) {
                temp = new Aves(Integer.parseInt(input[0]), input[1], input[2], Gender.parseGender(input[3]), Double.parseDouble(input[4]), Double.parseDouble(input[5]), input[6], Condition.parseCondition(input[7]));
            } else if (mapAnimalKingdom.get("reptiles").contains(input[1])) {
                temp = new Reptile(Integer.parseInt(input[0]), input[1], input[2], Gender.parseGender(input[3]), Double.parseDouble(input[4]), Double.parseDouble(input[5]), input[6], Condition.parseCondition(input[7]));
            } else {
                continue;
            }
            valid++;
            registerAnimal(temp, input[1]);
        }
        return valid;
    }

    @Override
    public long countInvalidRecords() {
        return total - valid;
    }

    public void registerAnimal(Animal performer, String type) {
        for (Map.Entry<String, List<String>> a : getMapAttraction().entrySet()) {
            if (a.getValue().contains(type)) {
                Attraction att = Attraction.find(a.getKey(), type);
                if (att == null) {
                    att = new Attraction(a.getKey(), type);
                }
                att.addPerformer(performer);
            }
        }
    }
}
