package javari.reader;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;

public class AttractionReader extends CsvReader {

    public AttractionReader(Path path) throws IOException {
        super(path);
    }

    @Override
    public long countValidRecords() {
        boolean isTrue = false;
        ArrayList<String> count = new ArrayList<>();

        for (String i : lines) {
            String[] input = i.split(COMMA);
            if (getMapAttraction().containsKey(input[1])) {
                if (getMapAttraction().get(input[1]).contains(input[0])) {
                    isTrue = true;
                }
            }
            if (isTrue == true) {
                if (!count.contains(input[1])) {
                    count.add(input[1]);
                }
            }
        }
        return (long)count.size();
    }

    @Override
    public long countInvalidRecords() {
        long invalidAtt = 0;

        for (String line : lines) {
            String[] input = line.split(COMMA);
            boolean isValid = (getMapAttraction().containsKey(input[1])) && getMapAttraction().get(input[1]).contains(input[0]);
            if (!isValid) {
                invalidAtt++;
            }
        }
        return invalidAtt;
    }
}
