package javari.reader;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

/**
 * This class represents the base class for reading all lines from a text
 * file that contains CSV data.
 *
 * @author Programming Foundations 2 Teaching Team
 * @author TODO If you make changes in this class, please write your name here
 * and describe the changes in the comment block
 */
public abstract class CsvReader {

    public static final String COMMA = ",";

    protected static final Map<String, List<String>> mapAnimal = createMapAnimal();
    protected static final Map<String, List<String>> mapAnimalKingdom = createMapAnimalKingdom();
    protected static final Map<String, List<String>> mapAttraction = createMapAttraction();
    private final Path file;
    protected final List<String> lines;

    /**
     * Defines the base constructor for instantiating an object of
     * {@code CsvReader}.
     *
     * @param file path object referring to a CSV file
     * @throws IOException if given file is not present or cannot be read
     *                     properly
     */
    public CsvReader(Path file) throws IOException {
        this.file = file;
        this.lines = Files.readAllLines(this.file, StandardCharsets.UTF_8);
    }

    public static Map<String, List<String>> getMapAnimal() {
        return mapAnimal;
    }

    public static Map<String, List<String>> getMapAttraction() {
        return mapAttraction;
    }

    public static Map<String, List<String>> getMapAnimalKingdom() {
        return mapAnimalKingdom;
    }

    private static Map<String, List<String>> createMapAnimal() {
        Map<String, List<String>> dict = new HashMap<>();
        dict.put("Explore the Mammals", new ArrayList<>(Arrays.asList("Hamster", "Lion", "Cat", "Whale")));
        dict.put("World of Aves", new ArrayList<>(Arrays.asList("Eagle", "Parrot")));
        dict.put("Reptillian Kingdom", new ArrayList<>(Arrays.asList("Snake")));
        return dict;
    }

    private static Map<String, List<String>> createMapAttraction() {
        Map<String, List<String>> dict = new HashMap<>();
        dict.put("Circles of Fires", new ArrayList<>(Arrays.asList("Lion", "Eagle", "Whale")));
        dict.put("Dancing Animals", new ArrayList<>(Arrays.asList("Snake", "Parrot", "Cat", "Hamster")));
        dict.put("Counting Masters", new ArrayList<>(Arrays.asList("Hamster", "Whale", "Parrot")));
        dict.put("Passionate Coders", new ArrayList<>(Arrays.asList("Hamster", "Cat", "Snake")));
        return dict;
    }

    private static Map<String, List<String>> createMapAnimalKingdom() {
        Map<String, List<String>> dict = new HashMap<>();
        dict.put("mammals", new ArrayList<>(Arrays.asList("Hamster", "Lion", "Cat", "Whale")));
        dict.put("aves", new ArrayList<>(Arrays.asList("Eagle", "Parrot")));
        dict.put("reptiles", new ArrayList<>(Arrays.asList("Snake")));
        return dict;
    }

    /**
     * Returns all line of text from CSV file as a list.
     *
     * @return
     */
    public List<String> getLines() {
        return lines;
    }

    /**
     * Counts the number of valid records from read CSV file.
     *
     * @return
     */
    public abstract long countValidRecords();

    /**
     * Counts the number of invalid records from read CSV file.
     *
     * @return
     */
    public abstract long countInvalidRecords();
}
