package javari.animal;

public class Mammal extends Animal {
    private String pregnant;

    public Mammal(Integer id, String type, String name, Gender gender, double length, double weight, String pregnant, Condition condition) {
        super(id, type, name, gender, length, weight, condition);
        this.pregnant = pregnant;
    }

    @Override
    protected boolean specificCondition() {
        if (!pregnant.equals("pregnant")) {
            if (getType().equals("Lion") && getGender() == Gender.FEMALE) {
                return false;
            }
            return true;
        }
        return false;
    }
}
