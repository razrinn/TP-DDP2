package javari.animal;

public class Aves extends Animal {
    private String layEgg;

    public Aves(Integer id, String type, String name, Gender gender, double length, double weight, String layEgg, Condition condition) {
        super(id, type, name, gender, length, weight, condition);
        this.layEgg = layEgg;
    }

    @Override
    protected boolean specificCondition() {
        return !layEgg.equals("lay egg");
    }
}
