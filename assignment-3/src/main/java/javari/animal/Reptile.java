package javari.animal;

public class Reptile extends Animal {
    private String tame;

    public Reptile(Integer id, String type, String name, Gender gender, double length, double weight, String tame, Condition condition) {
        super(id, type, name, gender, length, weight, condition);
        this.tame = tame;
    }

    @Override
    protected boolean specificCondition() {
        return tame.equals("tame");
    }
}
