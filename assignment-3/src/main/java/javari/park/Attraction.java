package javari.park;

import javari.animal.Animal;

import java.util.ArrayList;
import java.util.List;

public class Attraction implements SelectedAttraction {
    private String name;
    private String type;
    private List<Animal> performersList = new ArrayList<>();
    private static List<Attraction> attractionList = new ArrayList<>();

    public Attraction(String name, String type) {
        this.name = name;
        this.type = type;
        attractionList.add(this);
    }

    public static List<Attraction> getAttractionList() {
        return attractionList;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public List<Animal> getPerformers() {
        return performersList;
    }

    @Override
    public boolean addPerformer(Animal performer) {
        if (performer.isShowable()) {
            performersList.add(performer);
            return true;
        }
        return false;
    }

    public static Attraction find(String name, String type) {
        for (Attraction a : attractionList) {
            if (a.getName().equals(name) && a.getType().equals(type)) {
                return a;
            }
        }
        return null;
    }
}
