package javari.park;

import java.util.ArrayList;
import java.util.List;

public class Visitor implements Registration {
    private String visitorName;
    private int registrationId;
    private List<SelectedAttraction> selectedAttractions = new ArrayList<>();

    public Visitor(int registrationId, String visitorName) {
        this.registrationId = registrationId;
        this.visitorName = visitorName;
    }

    @Override
    public int getRegistrationId() {
        return registrationId;
    }

    @Override
    public String getVisitorName() {
        return visitorName;
    }

    @Override
    public String setVisitorName(String newName) {
        String oldName = visitorName;
        visitorName = newName;
        return oldName + " changed to " + visitorName;
    }

    @Override
    public List<SelectedAttraction> getSelectedAttractions() {
        return selectedAttractions;
    }

    @Override
    public boolean addSelectedAttraction(SelectedAttraction selected) {
        if (selected != null) {
            selectedAttractions.add(selected);
            return true;
        }
        return false;
    }
}
