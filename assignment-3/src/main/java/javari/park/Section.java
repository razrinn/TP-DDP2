package javari.park;

import java.util.ArrayList;
import java.util.List;

public class Section {
    private String name;
    private List<String> animal = new ArrayList<>();
    private static List<Section> sectionList = new ArrayList<>();

    public Section(String name) {
        this.name = name;
        sectionList.add(this);
    }

    public void addAnimal(String type) {
        animal.add(type);
    }

    public List<String> getAnimal() {
        return animal;
    }

    public String getName() {
        return name;
    }

    public static List<Section> getSectionList() {
        return sectionList;
    }
}
