import javari.animal.*;
import javari.reader.*;
import javari.park.*;
//import javari.writer.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;

public class A3Festival {
    public static void main(String[] args) {
        ArrayList<Registration> arrVisitor = new ArrayList<>();
        ArrayList<Attraction> attOption = new ArrayList<>();
        CsvReader category, record, attraction, sect;
        String type;
        long totalSect;
        int visitorId = 1;
        Scanner input = new Scanner(System.in);

        System.out.println("Welcome to Javari Park Festival - Registration Service!\n");
        System.out.print("... Opening default section database from data. \n");

        String path = "..\\..\\..\\data\\";
        while (true) {
            try {
                File csvAnimalCategories = new File(path + "animals_categories.csv");
                File csvAnimalRecords = new File(path + "animals_records.csv");
                File csvAttractions = new File(path + "animals_attractions.csv");
                File csvSections = new File(path + "animals_categories.csv");

                category = new CategoryReader(csvAnimalCategories.toPath());
                record = new RecordReader(csvAnimalRecords.toPath());
                attraction = new AttractionReader(csvAttractions.toPath());
                sect = new SectionReader(csvSections.toPath());

                totalSect = sect.countValidRecords();

                SectionReader secR = (SectionReader) sect;
                for (String i : secR.getSections()) {
                    Section j = new Section(i);
                }

                for (String cat : category.getLines()) {
                    String[] inp = cat.split(",");
                    for (Section s : Section.getSectionList()) {
                        if (inp[2].equals(s.getName())) {
                            s.addAnimal(inp[0]);
                        }
                    }
                }
                break;
            } catch (IOException e) {
                System.out.println("... File not found or incorrect file!\n");
                System.out.print("Please provide the source data path: ");
                path = input.nextLine();
            }
        }

        System.out.printf("Found %d valid sections and %d invalid sections%n", sect.countValidRecords(), sect.countInvalidRecords());
        System.out.printf("Found %d valid attractions and %d invalid attractions%n", attraction.countValidRecords(), attraction.countInvalidRecords());
        System.out.printf("Found %d valid animal categories and %d invalid animal categories%n", category.countValidRecords(), category.countInvalidRecords());
        System.out.printf("Found %d valid animal records and %d invalid animal records%n", record.countValidRecords(), record.countInvalidRecords());


        System.out.println("Welcome to Javari Park Festival - Registration Service!\n");
        System.out.println("Please answer the questions by typing the number. Type # if you want to return to the previous menu");
        mainMenu:
        while (true) {
            try {
                int index = 1;
                System.out.println("Javari Park has " + totalSect + " sections:");
                for (Section s : Section.getSectionList()) {
                    System.out.println(index++ + ". " + s.getName());
                }
                System.out.print("Please choose your preferred section (type the number); ");

                int cmdSec = Integer.parseInt(input.nextLine());
                if (cmdSec > totalSect) {
                    System.out.println("Input is not valid\n\n");
                    continue;
                }
                sectionMenu:
                while (true) {

                    index = 1;
                    System.out.println("\n--" + Section.getSectionList().get(cmdSec - 1).getName() + "--");
                    for (String typeSec : Section.getSectionList().get(cmdSec - 1).getAnimal()) {
                        System.out.println(index++ + ". " + typeSec);
                    }
                    System.out.print("Please choose your preferred animals (type the number): ");

                    String cmdAni = input.nextLine();
                    int intcmdAni = Integer.parseInt(cmdAni);

                    while (!cmdAni.equals("#")) {
                        type = Section.getSectionList().get(cmdSec - 1).getAnimal().get(intcmdAni - 1);
                        boolean isAvailable = false;
                        for (Attraction a : Attraction.getAttractionList()) {
                            if (a.getPerformers().size() > 0) {
                                for (Animal ani : a.getPerformers()) {
                                    if (ani.getType().equals(type)) {
                                        isAvailable = true;
                                    }
                                }
                            }
                        }
                        if (isAvailable) {
                            System.out.println("\n---" + type + "---\n" + "Attractions by " + type + ":");
                            index = 1;
                            for (Attraction a : Attraction.getAttractionList()) {
                                if (a.getPerformers().size() > 0) {
                                    if (a.getType().equals(type)) {
                                        attOption.add(a);
                                        System.out.println(index++ + ". " + a.getName());
                                    }
                                }
                            }
                        } else {
                            System.out.println("\nUnfortunately, no " + type + " can perform any attraction, please choose other animals\n");
                            continue;
                        }
                        System.out.print("Please choose your preferred attractions (type the number): ");
                        String cmdAtt = input.nextLine();

                        if (cmdAtt.equals("#")) continue ;

                        attractionMenu:
                        while (!cmdAtt.equals("#")) {
                            int intcmdAtt = Integer.parseInt(cmdAtt);
                            Attraction chosenAtt = attOption.get(intcmdAtt - 1);
                            System.out.print("\nWow, one more step, \nplease let us know your name: ");
                            String visitorName = input.nextLine();
                            System.out.println("\nYeay, final check! \nHere is your data, and the attraction you chose:" + "\nName: " +
                                    visitorName + "\nAttractions: " + chosenAtt.getName() + " -> " + type + "\nWith: ");

                            String[] arrPerformer = new String[chosenAtt.getPerformers().size()];

                            for (int i = 0; i < chosenAtt.getPerformers().size(); i++) {
                                arrPerformer[i] = chosenAtt.getPerformers().get(i).getName();

                            }
                            System.out.println(String.join(", ", arrPerformer));

                            System.out.println("\nIs the data correct? (Y/N): ");
                            String cmdDat = input.nextLine();

                            dataMenu:
                            while (!cmdDat.equals("#")){
                                boolean isRegistered = false;

                                if (cmdDat.equalsIgnoreCase("N")) {
                                    System.out.println("Unregistering ticket. Back to previous menu.\n");
                                    break;
                                }

                                if (!(cmdDat.equalsIgnoreCase("N") || cmdDat.equalsIgnoreCase("Y"))) {
                                    System.out.println("Input is not valid\n");
                                    break ;
                                }

                                if (!isRegistered) {
                                    Visitor v = new Visitor(visitorId++, visitorName);
                                    arrVisitor.add(v);
                                }
                                for (Registration r : arrVisitor) {
                                    if (r.getVisitorName().equals(visitorName)) {
                                        r.addSelectedAttraction(chosenAtt);
                                        }
                                }
                                System.out.println("Thank you for your interest. Would you like to register to other attractions? (Y/N)");
                                String cmdAno = input.nextLine();
                                if (cmdAno.equalsIgnoreCase("Y")) break sectionMenu;

                                if (!(cmdAno.equalsIgnoreCase("N") || cmdDat.equalsIgnoreCase("Y"))) {
                                    System.out.println("Input is not valid\n");
                                    break;
                                }
                                //for (Registration r : arrVisitor) {
                                //    RegistrationWriter.writeJson(r, Paths.get(path));
                                //}
                                break mainMenu;
                            }
                        }
                    }
                }

            } catch (Exception e) {
                System.out.println("Input is not valid\n");
            }
        }
    }
}
