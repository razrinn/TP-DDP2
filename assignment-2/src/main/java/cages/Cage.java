package cages;

import animals.*;

import java.util.*;

public class Cage {
    private ArrayList<Animal> cage = new ArrayList<Animal>();
    private Animal[][] levelArrange = new Animal[3][];
    private Animal[][] levelReArrange = new Animal[3][];
    private String location;

    public Cage(ArrayList<Animal> cage, boolean isPet) {
        this.cage = cage;
        if (isPet) {
            this.location = "indoor";
        } else {
            this.location = "outdoor";
        }
    }


    public void arrangeCage() {
        int size = this.cage.size();
        ArrayList<Animal> temp1 = new ArrayList<>();
        ArrayList<Animal> temp2 = new ArrayList<>();
        ArrayList<Animal> temp3 = new ArrayList<>();
        if (size > 0) {
            if (size == 1) {
                temp1.add(cage.get(0));
                levelArrange[0] = temp1.toArray(new Animal[temp1.size()]);
            } else if (size == 2) {
                for (int i = 0; i < 2; i++) {
                    if (i == 0) {
                        temp1.add(cage.get(i));
                    } else if (i == 1) {
                        temp2.add(cage.get(i));
                    }
                    levelArrange[0] = temp1.toArray(new Animal[temp1.size()]);
                    levelArrange[1] = temp2.toArray(new Animal[temp2.size()]);
                }
            } else {
                for (int i = 0; i < size; i++) {
                    if (i < size / 3) {
                        temp1.add(cage.get(i));
                    } else if (i >= size / 3 && i < size * 2 / 3) {
                        temp2.add(cage.get(i));
                    } else {
                        temp3.add(cage.get(i));
                    }
                }
                levelArrange[0] = temp1.toArray(new Animal[temp1.size()]);
                levelArrange[1] = temp2.toArray(new Animal[temp2.size()]);
                levelArrange[2] = temp3.toArray(new Animal[temp3.size()]);
            }

            System.out.println("Cage arrangement:");
            System.out.println("location: " + this.location);
            for (int i = 2; i >= 0; i--) {
                System.out.print("level " + (i + 1) + ": ");
                if (levelArrange[i] == null) {
                    System.out.println();
                    continue;
                }
                for (int j = 0; j < levelArrange[i].length; j++) {
                    System.out.print(levelArrange[i][j]);
                }
                System.out.println();
            }
            System.out.println();
        }
    }

    public void rearrangeCage() {
        int outside = levelArrange.length;
        Animal[] temp = levelArrange[1];
        levelArrange[1] = levelArrange[0];
        levelArrange[0] = levelArrange[2];
        levelArrange[2] = temp;

        System.out.println("After rearrangement...");
        for (int i = 2; i >= 0; i--) {
            System.out.print("level " + (i + 1) + ": ");
            if (levelArrange[i] != null) {
                for (int j = 0; j < levelArrange[i].length; j++) {
                    if (levelArrange[i][j] != null) System.out.print(levelArrange[i][j]);
                }
                System.out.println();

            }
        }
        System.out.println();
    }
}