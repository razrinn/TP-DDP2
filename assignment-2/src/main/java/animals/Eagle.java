package animals;

import animals.Animal;

public class Eagle extends Animal {
    public Eagle(String name, int length) {
        super(name, length);
        this.isPet = false;
        if (length < 75) this.cageType = "A";
        else if (length > 90) this.cageType = "C";
        else {
            this.cageType = "B";
        }
    }

    public void Fly() {
        System.out.println(this.name + " makes a voice: Kwaakk....\n" + "You hurt!");
    }
}