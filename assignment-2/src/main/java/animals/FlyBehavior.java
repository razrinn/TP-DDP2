package animals;

public interface FlyBehavior {
    void fly();
}
