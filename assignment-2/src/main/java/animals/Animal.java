package animals;

public class Animal {
    protected String name;
    protected int length;
    protected boolean isPet;
    protected String cageType;

    public Animal(String name, int length) {
        this.name = name;
        this.length = length;
        this.isPet = true;
    }

    public String getName() {
        return this.name;
    }

    public boolean getisPet() {
        return this.isPet;
    }

    public String getCageType() {
        return this.cageType;
    }

    public int getLength() {
        return this.length;
    }

    public String toString() {
        return getName() + " (" + getLength() + " - " + getCageType() + "),";
    }
}