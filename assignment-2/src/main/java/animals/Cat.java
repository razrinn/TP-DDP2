package animals;

import java.util.Random;

import animals.Animal;

public class Cat extends Animal {
    private static String[] voice = {"Miaaaw..", "Purr..", "Mwaw!", "Mraaawr!"};

    public Cat(String name, int length) {
        super(name, length);
        if (length < 45) this.cageType = "A";
        else if (length > 60) this.cageType = "C";
        else {
            this.cageType = "B";
        }
    }

    public void Brush() {
        System.out.println("Time to clean " + this.name + "'s fur\n" + this.name + " makes a voice : Nyaaan...");
    }

    public void Cuddle() {
        int rnd = new Random().nextInt(voice.length);
        System.out.println(this.name + " make a voice: " + voice[rnd]);
    }
}