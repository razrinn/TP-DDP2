package animals;

import animals.Animal;

public class Hamster extends Animal {
    public Hamster(String name, int length) {
        super(name, length);
        if (length < 45) this.cageType = "A";
        else if (length > 60) this.cageType = "C";
        else {
            this.cageType = "B";
        }
    }

    public void Gnaw() {
        System.out.println(this.name + " makes a voice: ngkkrit.. ngkkrrriiit");
    }

    public void Run() {
        System.out.println(this.name + " makes a voice: trrr... trrr...");
    }
}