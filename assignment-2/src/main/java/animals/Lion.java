package animals;

import animals.Animal;

public class Lion extends Animal {
    public Lion(String name, int length) {
        super(name, length);
        this.isPet = false;
        if (length < 75) this.cageType = "A";
        else if (length > 90) this.cageType = "C";
        else {
            this.cageType = "B";
        }
    }

    public void Hunt() {
        System.out.println("Lion is hunting...\n" + this.name + " makes a voice: err...!");
    }

    public void Brush() {
        System.out.println("Clean the lion's mane..\n" + this.name + " makes a voice: Hauhhmm!");
    }

    public void Disturb() {
        System.out.println(this.name + " makes a voice: HAUHHMM!");
    }
}