package animals;

import animals.Animal;

public class Parrot extends Animal {
    public Parrot(String name, int length) {
        super(name, length);
        if (length < 45) this.cageType = "A";
        else if (length > 60) this.cageType = "C";
        else {
            this.cageType = "B";
        }
    }

    public void Fly() {
        System.out.println(this.name + " makes a voice: FLYYY...");
    }

    public void Imitate(String conv) {
        System.out.println(this.name + " says: " + conv.toUpperCase() + "\nBack to the office!\n");
    }

    public void Nothing() {
        System.out.println(this.name + " says: HM?");
    }
}