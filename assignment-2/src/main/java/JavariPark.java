import animals.*;
import cages.Cage;

import java.util.*;

public class JavariPark {
    private static ArrayList<Animal> arrliCat = new ArrayList<Animal>();
    private static ArrayList<Animal> arrliHamster = new ArrayList<Animal>();
    private static ArrayList<Animal> arrliParrot = new ArrayList<Animal>();
    private static ArrayList<Animal> arrliEagle = new ArrayList<Animal>();
    private static ArrayList<Animal> arrliLion = new ArrayList<Animal>();
    private static Cage cages[] = new Cage[5];

    public static boolean checkName(ArrayList<Animal> arrli, String name) {
        for (Animal anim : arrli) {
            if (anim.getName().equals(name)) return true;
        }
        return false;
    }

    public static int findIndex(ArrayList<Animal> arrli, String name) {
        for (int i = 0; i < arrli.size(); i++) {
            if (arrli.get(i).getName().equals(name)) return i;
        }
        return -1;
    }

    public static void main(String[] args) {
        //instantiating
        Scanner input = new Scanner(System.in);
        String[] animal = {"cat", "lion", "eagle", "parrot", "hamster"};
        ArrayList<ArrayList<Animal>> arrAnim = new ArrayList<ArrayList<Animal>>();
        arrAnim.add(arrliCat);
        arrAnim.add(arrliLion);
        arrAnim.add(arrliEagle);
        arrAnim.add(arrliParrot);
        arrAnim.add(arrliHamster);
        System.out.println("Welcome to Javari Park!\nInput the number of animals");
        for (String name : animal) {
            System.out.print(name + ": ");
            int totalAnimal = Integer.parseInt(input.next());
            if (totalAnimal > 0) {
                System.out.println("Provide the information of " + name + "(s):");
            }
            if (totalAnimal > 0) {
                String[] arrTotal = input.next().split(",");
                for (String data : arrTotal) {
                    String[] dataSplit = data.split("\\|");
                    String nameAnim = dataSplit[0];
                    int lengthAnim = Integer.parseInt(dataSplit[1]);
                    if (name.equals("cat")) {
                        Animal temp = new Cat(nameAnim, lengthAnim);
                        arrliCat.add(temp);
                    } else if (name.equals("lion")) {
                        Animal temp = new Lion(nameAnim, lengthAnim);
                        arrliLion.add(temp);
                    } else if (name.equals("eagle")) {
                        Animal temp = new Eagle(nameAnim, lengthAnim);
                        arrliEagle.add(temp);
                    } else if (name.equals("parrot")) {
                        Animal temp = new Parrot(nameAnim, lengthAnim);
                        arrliParrot.add(temp);
                    } else if (name.equals("hamster")) {
                        Animal temp = new Hamster(nameAnim, lengthAnim);
                        arrliHamster.add(temp);
                    }
                }
            }
        }
        System.out.println("Animals have been successfully recorded!");

        ////////////
        System.out.println("\n=============================================");
        for (int i = 0; i < arrAnim.size(); i++) {
            if (arrAnim.get(i).size() == 0) continue;
            if (arrAnim.get(i) != null) {
                cages[i] = new Cage(arrAnim.get(i), arrAnim.get(i).get(0).getisPet());
                cages[i].arrangeCage();
                cages[i].rearrangeCage();
            }
        }
        animal[2] = "parrot";
        animal[3] = "eagle";
        System.out.println("NUMBER OF ANIMALS:");
        for (int i = 0; i < animal.length; i++) {
            System.out.println(animal[i] + ":" + arrAnim.get(i).size());
        }


        /////////////
        System.out.println("\n=============================================");

        boolean loop = true;
        do {
            System.out.println("Which animal you want to visit?" + "\n(1: Cat, 2: Eagle, 3: Hamster, 4: Parrot, 5: Lion, 99: Exit");
            int intVisit = Integer.parseInt(input.next());
            if (intVisit == 1) {
                System.out.print("Mention the name of cat you want to visit: ");
                String aniName = input.next();
                if (checkName(arrliCat, aniName) == true) {
                    int index = findIndex(arrliCat, aniName);
                    System.out.println("You are visitting " + aniName + " (cat) now, what would you like to do?" + "\n1: Brush the fur 2: Cuddle");
                    int intAct = Integer.parseInt(input.next());
                    if (intAct == 1) {
                        Cat z = (Cat) arrliCat.get(index);
                        z.Brush();
                    } else if (intAct == 2) {
                        Cat z = (Cat) arrliCat.get(index);
                        z.Cuddle();
                    } else {
                        System.out.println("You do nothing...");
                    }
                    System.out.println("Back to the office!\n");
                } else {
                    System.out.println("There is no cat with that name! Back to the office!\n");
                }
            } else if (intVisit == 2) {
                System.out.print("Mention the name of Eagle you want to visit: ");
                String aniName = input.next();
                if (checkName(arrliEagle, aniName) == true) {
                    int index = findIndex(arrliEagle, aniName);
                    System.out.println("You are visitting " + aniName + " (eagle) now, what would you like to do?" + "\n1: Order to fly");
                    int intAct = Integer.parseInt(input.next());
                    if (intAct == 1) {
                        Eagle z = (Eagle) arrliEagle.get(index);
                        z.Fly();
                    } else {
                        System.out.println("You do nothing...");
                    }
                    System.out.println("Back to the office!\n");
                } else {
                    System.out.println("There is no eagle with that name! Back to the office!\n");
                }
            } else if (intVisit == 3) {
                System.out.print("Mention the name of Hamster you want to visit: ");
                String aniName = input.next();
                if (checkName(arrliHamster, aniName) == true) {
                    int index = findIndex(arrliHamster, aniName);
                    System.out.println("You are visitting " + aniName + " (hamster) now, what would you like to do?" + "\n1: See it gnawing 2: Order to run in the hamster wheel");
                    int intAct = Integer.parseInt(input.next());
                    if (intAct == 1) {
                        Hamster z = (Hamster) arrliHamster.get(index);
                        z.Gnaw();
                    } else if (intAct == 2) {
                        Hamster z = (Hamster) arrliHamster.get(index);
                        z.Run();
                    } else {
                        System.out.println("You do nothing...");
                    }
                    System.out.println("Back to the office!\n");
                } else {
                    System.out.println("There is no hamster with that name! Back to the office!\n");
                }
            } else if (intVisit == 4) {
                System.out.print("Mention the name of Parrot you want to visit: ");
                String aniName = input.next();
                if (checkName(arrliParrot, aniName) == true) {
                    int index = findIndex(arrliParrot, aniName);
                    System.out.println("You are visitting " + aniName + " (parrot) now, what would you like to do?" + "\n1: Order to fly 2: Do conversation");
                    int intAct = Integer.parseInt(input.next());
                    input.nextLine();
                    if (intAct == 1) {
                        Parrot z = (Parrot) arrliParrot.get(index);
                        z.Fly();
                    } else if (intAct == 2) {
                        System.out.print("You say: ");
                        String conv = input.nextLine();
                        Parrot z = (Parrot) arrliParrot.get(index);
                        z.Imitate(conv);
                    } else {
                        Parrot z = (Parrot) arrliParrot.get(index);
                        z.Nothing();
                        System.out.println("Back to the office!\n");
                    }
                } else {
                    System.out.println("There is no parrot with that name! Back to the office!\n");
                }
            } else if (intVisit == 5) {
                System.out.print("Mention the name of Lion you want to visit: ");
                String aniName = input.next();
                if (checkName(arrliLion, aniName) == true) {
                    int index = findIndex(arrliLion, aniName);
                    System.out.println("You are visitting " + aniName + " (lion) now, what would you like to do?" + "\n1: See it hunting 2: Brush the mane 3: Disturb it");
                    int intAct = Integer.parseInt(input.next());
                    if (intAct == 1) {
                        Lion z = (Lion) arrliLion.get(index);
                        z.Hunt();
                    } else if (intAct == 2) {
                        Lion z = (Lion) arrliLion.get(index);
                        z.Brush();
                    } else if (intAct == 3) {
                        Lion z = (Lion) arrliLion.get(index);
                        z.Disturb();
                    } else {
                        System.out.println("You do nothing...");
                    }
                    System.out.println("Back to the office!\n");
                } else {
                    System.out.println("There is no lion with that name! Back to the office!\n");
                }
            } else if (intVisit == 99) {
                loop = false;
            }
        } while (loop);
    }
}