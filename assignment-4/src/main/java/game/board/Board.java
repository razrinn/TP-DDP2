package game.board;

import game.card.Card;
import game.listener.Listener;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Class Board for the board of the cards
 */
public class Board {
    /**
     * var mainPanel to save the main panel that contains cards Panel
     */
    private JPanel mainPanel;
    /**
     * var cardsPanel to save the panel that contains card buttons
     */
    private JPanel cardsPanel;
    /**
     * var tryLabel to make a label that counts how many times the player has opened the cards
     */
    private JLabel tryLabel;
    /**
     * var highLabel to make a label that shows the lowest turn record
     */
    private JLabel highLabel;
    /**
     * var tryCounter to save how many times player has opened the cards
     */
    private int tryCounter;
    /**
     * var stepCounter to save how many times player has opened the cards in one turn (max. 2)
     */
    private int stepCounter;
    /**
     * var lowestRecord to save the lowest attempt
     */
    private int lowestRecord;
    /**
     * var cards to save the cards that have been initialized
     */
    private ArrayList<Card> cards = new ArrayList<>();
    /**
     * var command to save the ActionListener for doing something after clicking
     */
    private Listener command = new Listener(this);

    /**
     * Class constructor will create a main panel that contains cardPanel, labels and buttons
     */
    public Board() {
        mainPanel = new JPanel();
        mainPanel.setLayout(new BorderLayout());
        JOptionPane.showMessageDialog(cardsPanel, "                   Welcome to \"Remember the Character\" game" +
                "\nYou need to click on 2 identical cards of 18 pair of cards to win the game \n                    " +
                "You have 5 seconds to memorize the cards\n                                                " +
                "Good Luck!", "Welcome!", JOptionPane.INFORMATION_MESSAGE);
        cardsPanel = createCardButton();
        mainPanel.add(cardsPanel, BorderLayout.PAGE_START);
        tryLabel = new JLabel("Number of tries: " + tryCounter);
        mainPanel.add(tryLabel, BorderLayout.WEST);
        highLabel = new JLabel("         Current lowest record: " + lowestRecord);
        mainPanel.add(highLabel, BorderLayout.CENTER);
        mainPanel.add(createHelperButton(), BorderLayout.EAST);
    }

    /**
     * to create a card buttons that has unique id's for each 2 cards and shuffle it
     *
     * @return JPanel value that used in constructor
     */
    private JPanel createCardButton() {
        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(6, 6));
        int j = 1;
        for (int i = 1; i <= 36; i++) {
            if (j > 18) j = 1;
            Card temp = new Card(j);
            temp.addActionListener(command);
            temp.setEnabled(true);
            cards.add(temp);
            j++;
        }
        Collections.shuffle(cards);
        for (Card b : cards) {
            panel.add(b);
        }
        newGame();
        return panel;
    }

    /**
     * to create helper button like restart and exit
     *
     * @return JPanel value that used in constructor
     */
    private JPanel createHelperButton() {
        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(1, 2));
        JButton restart = new JButton("Restart");
        restart.addActionListener(e -> restart());
        JButton exit = new JButton("Exit");
        exit.addActionListener(e -> System.exit(0));
        panel.add(restart);
        panel.add(exit);
        return panel;
    }

    /**
     * to open all cards before the game start in 5 seconds
     */
    private void newGame() {
        for (Card c : cards) {
            c.openCard();
        }
        Timer t = new Timer(5000, ae -> {
            for (Card c : cards) {
                if (!c.isCorrect()) c.closeCard();
            }
        });
        t.setRepeats(false);
        t.start();
    }

    /**
     * to do when the player has win
     */
    public void win() {
        setLowestRecord();
        JOptionPane.showMessageDialog(mainPanel, "           CONGRATULATIONS!!\nYOU WON THE GAME WITH " + tryCounter + " STEPS", "CONGRATULATIONS!", JOptionPane.INFORMATION_MESSAGE);
    }

    /**
     * to restart the game
     * remove current card panel and add a new one then reset the counters
     */
    private void restart() {
        int restart = JOptionPane.showConfirmDialog(mainPanel, "Are you sure to restart the game?", "Restart", JOptionPane.YES_NO_OPTION);
        if (restart == JOptionPane.YES_OPTION) {
            mainPanel.remove(cardsPanel);
            mainPanel.revalidate();
            mainPanel.repaint();
            cards.clear();
            cards.clear();
            mainPanel.add(createCardButton(), BorderLayout.PAGE_START);
            resetStepCounter();
            resetCounter();
        }
    }

    /**
     * to set the record of the player
     */
    private void setLowestRecord() {
        if (lowestRecord == 0) lowestRecord = tryCounter;
        else {
            if (tryCounter < lowestRecord) lowestRecord = tryCounter;
        }
        highLabel.setText("         Current lowest record: " + lowestRecord);
    }

    /**
     * to reset the attempt counter
     */
    private void resetCounter() {
        tryCounter = 0;
        tryLabel.setText("Number of tries: " + tryCounter);
    }

    /**
     * to increment the attempt counter
     */
    public void incCounter() {
        tryLabel.setText("Number of tries: " + ++tryCounter);
    }

    /**
     * to increment the step counter
     */
    public void incStepCounter() {
        stepCounter++;
    }

    /**
     * to reset the step counter
     */
    public void resetStepCounter() {
        stepCounter = 0;
    }

    /**
     * @return ArrayList<Cards> value of cards arraylist
     */
    public ArrayList<Card> getCards() {
        return cards;
    }

    /**
     * @return int value of steps in 1 turn
     */
    public int getStepCounter() {
        return stepCounter;
    }

    /**
     * @return JPanel value of main panel
     */
    public JPanel getMainPanel() {
        return mainPanel;
    }
}
