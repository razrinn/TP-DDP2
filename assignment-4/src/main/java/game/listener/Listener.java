package game.listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import game.card.Card;
import game.board.Board;

import javax.swing.*;

/**
 * Class Listener to get all the user interaction
 */
public class Listener implements ActionListener {
    /**
     * to save the processed board
     */
    private Board board;
    /**
     * to save the first opened card
     */
    private static Card firstCard;
    /**
     * to save the second opened card
     */
    private Card secondCard;

    /**
     * Constructor to assign board to the processed one
     * @param board
     */
    public Listener(Board board) {
        this.board = board;
    }

    /**
     * to do everything when clicked
     * @param e is the action event
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        secondCard = (Card) e.getSource();
        secondCard.setClicked(true);
        int stepCounter = board.getStepCounter();
        if (stepCounter == 0) runNormal();
        else if (stepCounter == 1) runNormal1();
        secondCard.setClicked(false);
        checkWin();
    }

    /**
     * to check whether the player has win or not
     */
    private void checkWin() {
        boolean isWin = true;
        for (Card c : board.getCards()) {
            isWin &= c.isCorrect();
        }
        if (isWin) board.win();
    }

    /**
     * to run the game at the first click of card
     */
    private void runNormal() {
        if (secondCard.isClicked()) secondCard.openCard();
        firstCard = secondCard;
        secondCard.setClicked(false);
        firstCard.setClicked(false);
        board.incStepCounter();
    }

    /**
     * to run the game at the second click of card
     */
    private void runNormal1() {
        if (secondCard.isClicked()) secondCard.openCard();
        if (secondCard.equals(firstCard)) {
            secondCard.closeCard();
            secondCard.setClicked(false);
            board.resetStepCounter();
            board.incCounter();
            return;
        }
        if (firstCard.equalsTo(secondCard)) {
            correct();
            return;
        }
        secondCard.setClicked(false);
        firstCard.setClicked(false);

        Timer t = new Timer(600, ae -> {
            for (Card c : board.getCards()) {
                if (!c.isCorrect()) c.closeCard();
            }
        });
        t.setRepeats(false);
        t.start();
        board.incCounter();
        board.resetStepCounter();
    }

    /**
     * to make the card unavailable to click and set its condition when the 2 cards matched
     */
    private void correct() {
        firstCard.setEnabled(false);
        firstCard.setCorrect(true);
        secondCard.setEnabled(false);
        secondCard.setCorrect(true);
        board.incCounter();
        board.resetStepCounter();
    }

}
