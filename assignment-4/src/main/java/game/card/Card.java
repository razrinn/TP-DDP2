package game.card;

import javax.swing.*;
import java.awt.*;

/**
 * Class card for managing the each card of the board
 */
public class Card extends JButton {
    /**
     * var id to save the id of the card
     */
    private int id;
    /**
     * var open to save the opened card image
     */
    private ImageIcon open;
    /**
     * var isClicked to save whether the button is clicked or not
     */
    private boolean isClicked;
    /**
     * var isCorrect to save whether the button is already correct or not
     */
    private boolean isCorrect;

    /**
     * var CLOSE to save the closed card image
     */
    private static final ImageIcon CLOSE = resizeIcon(new ImageIcon("F:\\CODING\\TP-DDP2\\assignment-4\\src\\img\\closed.jpg"));

    /**
     * Class constructor will save id, open, and set isClicked to false
     *
     * @param id is the id of the each card
     */
    public Card(int id) {
        this.id = id;
        this.open = resizeIcon(new ImageIcon("F:\\CODING\\TP-DDP2\\assignment-4\\src\\img\\" + id + ".png"));
        this.isClicked = false;
        setPreferredSize(new Dimension(120, 120));
    }

    /**
     * to check this card's Id to the other card's Id
     *
     * @param next is the other card to be checked
     * @return true if both id are same, false if they're not
     */
    public boolean equalsTo(Card next) {
        return id == next.id;
    }

    /**
     * to open the card
     */
    public void openCard() {
        this.setIcon(open);
    }

    /**
     * to resize the image size of the var icon
     *
     * @param icon is the image you want to resize
     * @return ImageIcon value of resized image
     */
    private static ImageIcon resizeIcon(ImageIcon icon) {
        Image img = icon.getImage();
        Image resizedImage = img.getScaledInstance(120, 120, java.awt.Image.SCALE_SMOOTH);
        return new ImageIcon(resizedImage);
    }

    /**
     * to change the image of the card to var CLOSE
     */
    public void closeCard() {
        this.setIcon(CLOSE);
    }

    /**
     * to change the status of the card is clicked or not
     *
     * @param isCorrect is the boolean value to change
     */
    public void setCorrect(boolean isCorrect) {
        this.isCorrect = isCorrect;
    }

    /**
     * to change the status of the card is clicked or not
     *
     * @param bool is the boolean value to change
     */
    public void setClicked(boolean bool) {
        isClicked = bool;
    }

    /**
     * @return boolean value of var isClicked
     */
    public boolean isClicked() {
        return isClicked;
    }

    /**
     * @return boolean value of var isCorrect
     */
    public boolean isCorrect() {
        return isCorrect;
    }
}
