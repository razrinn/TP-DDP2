import javax.swing.*;

import game.board.Board;

import java.awt.*;

/**
 * Class main to run the game
 */
public class MemoryGameMain {
    /**
     * Constructor for creating the game window and all its components
     */
    private MemoryGameMain() {
        JFrame frame = new JFrame("Remember the teams! by Ray");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(720, 800);
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        frame.setLocation(dim.width / 2 - frame.getSize().width / 2, dim.height / 2 - frame.getSize().height / 2);
        Board panel = new Board();
        frame.getContentPane().add(panel.getMainPanel());
        frame.setResizable(false);
        frame.setVisible(true);
    }

    /**
     * Main method to run the game
     */
    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                MemoryGameMain game = new MemoryGameMain();
            }
        });
    }
}
